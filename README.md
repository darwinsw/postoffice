# Bradipo #

| Branch | Pipeline Status |Code Coverage |
| ------------- |:--------------|---|
| master      | [![pipeline status](https://gitlab.com/darwinsw/postoffice/badges/master/pipeline.svg)](https://gitlab.com/darwinsw/postoffice/commits/master) | [![coverage](https://gitlab.com/darwinsw/postoffice/badges/master/coverage.svg)](https://darwinsw.gitlab.io/postoffice/coverage/master/html-report/index.htm)|
| release | [![pipeline status](https://gitlab.com/darwinsw/postoffice/badges/release/pipeline.svg)](https://gitlab.com/darwinsw/postoffice/commits/release) | [![coverage](https://gitlab.com/darwinsw/postoffice/badges/release/coverage.svg)](https://darwinsw.gitlab.io/postoffice/coverage/release/html-report/index.htm)|
| ci-dev | [![pipeline status](https://gitlab.com/darwinsw/postoffice/badges/ci-dev/pipeline.svg)](https://gitlab.com/darwinsw/postoffice/commits/ci-dev) | [![coverage](https://gitlab.com/darwinsw/postoffice/badges/ci-dev/coverage.svg)](https://darwinsw.gitlab.io/postoffice/coverage/ci-dev/html-report/index.htm)|

# Summary

A [*.Net Standard*](https://docs.microsoft.com/en-us/dotnet/standard/net-standard) compliant .Net library, providing a simple *facade* over the `System.Net.Mail` API.

This is a fork based on the previous work publihed as [Plastiline.PostOffice](https://github.com/codicePlastico/Plastiline.PostOffice/): the purpose of forking is to move the project to [GitLab](https://gitlab.com), adopting GitLab's rich CI and move faster toward new enhancements.

# Getting Started
In order to take advantage of `System.Net.Mail` API in an user-friendly way, you need to instantiate `PostOfficeService` class and pass a `SendLetterCommand` to its `SendMail` method. As you can see in [`ScenarioTest`](../../blob/master/PostOffice.Tests/ScenarioTest%20.cs), the basic usage appear like the following code snippet:
```
var service = new PostOfficeService(theConfigurationProvider, theTemplateProvider, theAttachmentProvider, theTemplateRenderer);
service.SendMail(new SendLetterCommand {
    To = new[] { "test@test.com" },
    LetterTemplate = "TheTemplate",
    Data = new Dictionary<string, object> {
        {"key0", "value0"},
        {"key1", "value1"}
    },
    Attachments = new Attachment[] {}
});
```
## Configuration patterns
Alternatively, you can provide directly a `SmtpConfigurationInstance` to `PostOfficeService` or provide a _lambda_ returning it:
```
var service = new PostOfficeService(new SmtpConfiguration { ... }, theTemplateProvider);
```
and
```
var service = new PostOfficeService(() => new SmtpConfiguration { ... }, theTemplateProvider);
```
 are both valid configuration scenarios.
 
## How it works

`PostOffice` uses 

  1. `theConfigurationProvider` (instance of a class implementing `SmtpConfigurationProvider` to obtain the SMTP server configuration)
  2. `theTemplateProvider` (instance of a class implementing `TemplateProvider`) to obtain an instance of `EmailTemplate`: an `EmailTemplate` provides info about subject, body and *from* od the mail to be sent
  3. `theAttachmentProvider` (instance of a class implementing `AttachmentProvider`) to obtain attachments' content
  4. `theTemplateRenderer` (instance of a class implementing `TemplateRenderer`) to render email templates applying input data 
     
The subject and the body of `EmailTemplate`s can contain variables:

  1. if `DefaultRenderer` is in use, you can use plain variables, referring them like `{key0}`: variables are replaced with the values provided by `SendLetterCommand.Data` (applying `ToString` method to provided values).
  2. if `ScribanTemplateRenderer` is in use, you can use the full [Scriban's](https://github.com/lunet-io/scriban) syntax and feature in both email body and email subject
  3. you can write your own `TemplateRenderer` implementation, in order to integrate PostOffice for you preferred templating engine   



# Usage Patterns
From a design viewpoint, the tipical usage is to inject a `PostOfficeService` instance into the class which should be able to send emails and to use it as showed above.
This `PostOfficeService`'s client can be e.g. an asynchronous consumer of a domain event or of an internal command: it converts message-provided data into an instance of `SendLetterCommand`, passing it to the `PostOfficeService` instance.

## PostOffice and IoC container
PostOffice is an IoC friendly library: you can plug its features into you application simply registering an application component for both `PostOfficeService` and its collaborators.
Using [Autofac](https://autofac.org/) as your IoC container, you can e.g. registering the stuff you need as follows:
```
public class PostOfficeModule : IModule {
   protected override void Load(ContainerBuilder builder) {
       builder.RegisterType<PostOfficeService>();
       builder.RegisterType<YourSmtpConfigurationProvider>().As<SmtpConfigurationProvider>();
       builder.RegisterType<YourTemplateProvider>().As<TemplateProvider>();
       builder.RegisterType<YourAttachmentProvider>().As<AttachmentProvider>();
       builder.RegisterType<DefaultRenderer>().As<TemplateRenderer>();
   }
}
```
# Plugging your own collaborators implementation
`TemplateProvider`, `SmtpConfigurationProvider`, `AttachmentProvider`, and (as already explained) `TemplateRenderer` are the interfaces you need to implement in order to wire you application-specific behaviour as `PostOfficeService`'s collaborators.

## Naming convention
You can wonder about the naming convention PostOffice adopts for interfaces: interfaces listed above have no `I` prefix in their names.
The reason is I think it's a violation of loose coupling and [Single Responsibility Principle](https://en.wikipedia.org/wiki/Single_responsibility_principle) showing syntactical details (as the *type* of the type is) in the name of a type.
So, if you doesn't use something like an `iCount` convention for `int` variables, I think you should'n use the broadly adopted convention of an `I` prefix in interface names or the convention of an `Impl` suffix in class names. 