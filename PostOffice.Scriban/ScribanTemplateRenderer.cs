﻿using System;
using System.Collections.Generic;
using PostOffice.Templating;
using Scriban;

namespace PostOffice.Scriban {
    public class ScribanTemplateRenderer : TemplateRenderer {
        public string Render(string template, IDictionary<string, object> data) {
            var tpl = Template.Parse(template);
            var result = tpl.Render(data, member => member.Name.UncapFirst());
            return result;
        }
    }

    public static class Helper {
        public static string UncapFirst(this string val) {
            return val.Substring(0, 1).ToLowerInvariant() + val.Substring(1);
        }
    }
}