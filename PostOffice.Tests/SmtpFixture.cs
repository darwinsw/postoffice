using System;
using netDumbster.smtp;
using Xunit;

namespace PostOffice.Tests {
    [CollectionDefinition("Smtp collection")]
    public class SmtpCollection : ICollectionFixture<SmtpFixture> {
    }
    
    public class SmtpFixture : IDisposable {
        public const int SmtpPort = 10000;

        public SimpleSmtpServer Server { get; private set; }

        public SmtpFixture() {
            Server = SimpleSmtpServer.Start(SmtpPort);
        }


        public void Dispose() {
            Server.Stop();
        }
    }
}