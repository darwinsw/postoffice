using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHamcrest;
using PostOffice.Api;
using PostOffice.Attachments;
using PostOffice.Configuration;
using PostOffice.Scriban;
using PostOffice.Sender;
using PostOffice.Templating;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;
using SmtpConfigurationProvider = PostOffice.Configuration.SmtpConfigurationProvider;
using TemplateProvider = PostOffice.Templating.TemplateProvider;

namespace PostOffice.Tests {
    [Collection("Smtp collection")]
    public class AsynchronousScenarioTest : IDisposable, TemplateProvider, SmtpConfigurationProvider,
        AttachmentProvider {
        private const string LoremIpsum =
            @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut 
labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        private readonly SmtpFixture fixture;
        private readonly Api.PostOffice service;

        private static readonly SmtpConfiguration Configuration = new SmtpConfiguration {
            Host = "localhost",
            Port = SmtpFixture.SmtpPort,
            EnableSsl = false,
            RequiresAuthentication = false
        };

        public AsynchronousScenarioTest(SmtpFixture fixture) {
            this.fixture = fixture;
            this.service = new PostOfficeService(this, this, this, new DefaultRenderer());
        }

        [Fact]
        public async Task ShouldSendAnEmail() {
            await service.SendMail(MailWithoutAttachments());
            Assert.That(fixture.Server.ReceivedEmailCount, Is.EqualTo(1));
        }

        [Fact]
        public async Task ShouldSendAnEmailUsingProvidedTemplateRenderer() {
            Api.PostOffice postOffice = new PostOfficeService(Configuration, new ScribanFakeTemplateProvider(), this,
                new ScribanTemplateRenderer());
            await service.SendMail(MailWithoutAttachments());
            Assert.That(fixture.Server.ReceivedEmail[0].MessageParts[0].BodyData, Is.EqualTo("Sample body value0 19"));
        }

        [Fact]
        public async Task CanConfigurePostOfficeThroughFixedConfiguration() {
            Api.PostOffice postOffice = new PostOfficeService(Configuration, this, this, new DefaultRenderer());
            await service.SendMail(MailWithoutAttachments());
            Assert.That(fixture.Server.ReceivedEmailCount, Is.EqualTo(1));
        }

        [Fact]
        public async Task CanConfigurePostOfficeThroughDelegate() {
            Api.PostOffice postOffice =
                new PostOfficeService(() => Task.FromResult(Configuration), this, this, new DefaultRenderer());
            await service.SendMail(MailWithoutAttachments());
            Assert.That(fixture.Server.ReceivedEmailCount, Is.EqualTo(1));
        }

        [Fact]
        public async Task ShouldProcessEmailBody() {
            await service.SendMail(MailWithoutAttachments());
            Assert.That(fixture.Server.ReceivedEmail[0].MessageParts[0].BodyData, Is.EqualTo("Sample body value0 19"));
        }

        [Fact]
        public async Task ShouldUseCcsAndBccs() {
            await service.SendMail(new SendLetterCommand {
                To = new[] {"test0@test.com"},
                Cc = new[] {"test1@test.cpom", "test1@test.cpom"},
                Bcc = new[] {"test4@test.cpom", "test5@test.cpom"},
                LetterTemplate = "TheTemplate",
                Data = new Dictionary<string, object> {
                    {"key0", "value0"},
                    {"key1", "value1"}
                },
                Attachments = new Attachment[] { }
            });
            Assert.That(fixture.Server.ReceivedEmail[0].ToAddresses, Is.OfLength(5));
        }

        [Fact]
        public async Task ShouldIncludeAttachments() {
            await service.SendMail(new SendLetterCommand {
                To = new[] {"test@test.com"},
                LetterTemplate = "TheTemplate",
                Data = new Dictionary<string, object> {
                    {"key0", "value0"},
                    {"key1", "value1"}
                },
                Attachments = new[] {
                    Attachment.From("theAttachment.txt","theAttachmentAlias.txt")
                }
            });
            Assert.That(AttachmentContent(), Is.EqualTo(LoremIpsum));
        }

        [Fact]
        public async Task ShouldIncludeLinkedResource() {
            await service.SendMail(new SendLetterCommand {
                To = new[] {"test@test.com"},
                LetterTemplate = "TheTemplate",
                Data = new Dictionary<string, object> {
                    {"key0", "value0"},
                    {"key1", "value1"}
                },
                Attachments = new[] {
                    Attachment.From("theAttachment.txt","theAttachmentAlias.txt")
                },
                EmbeddedAttachments = new[] {EmbeddedAttachment.From("theId", "theName", "image/png")}
            });
            Assert.That(fixture.Server.ReceivedEmail[0].MessageParts.Length, Is.EqualTo(4));
        }

        private string AttachmentContent() {
            return Encoding.UTF8.GetString(
                System.Convert.FromBase64String(fixture.Server.ReceivedEmail.Single().MessageParts[1].BodyData));
        }

        public void Dispose() {
            fixture.Server.ClearReceivedEmail();
        }

        public Task<EmailTemplate> ProvideTemplate(string templateName) {
            return Task.FromResult(new EmailTemplate {
                Body = "Sample body {key0} {key1}",
                From = "tet@test.com",
                Subject = "Sample subject",
                HasHtmlContent = true
            });
        }

        public Task<SmtpConfiguration> ProvideConfiguration() {
            return Task.FromResult(Configuration);
        }

        public Task<Stream> ProvideAttachment(string attachmentId) {
            return Task.FromResult(new MemoryStream(Encoding.UTF8.GetBytes(LoremIpsum)) as Stream);
        }

        private static SendLetterCommand MailWithoutAttachments() {
            return new SendLetterCommand {
                To = new[] {"test@test.com"},
                LetterTemplate = "TheTemplate",
                Data = new Dictionary<string, object> {
                    {"key0", "value0"},
                    {"key1", 19}
                },
                Attachments = new Attachment[] { }
            };
        }
    }

    public class ScribanFakeTemplateProvider : TemplateProvider {
        public Task<EmailTemplate> ProvideTemplate(string templateName) {
            return Task.FromResult(new EmailTemplate {
                Body = "Sample body {{key0}} {{key1}}",
                From = "tet@test.com",
                Subject = "Sample subject",
                HasHtmlContent = true
            });
        }
    }
}