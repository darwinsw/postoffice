using System.Collections.Generic;
using NHamcrest;
using PostOffice.Templating;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace PostOffice.Tests {
    public class DefaultTemplateRendererTest {
        private readonly TemplateRenderer renderer = new DefaultRenderer();

        [Fact]
        public void RenderPlainStringVariables() {
            string template = "Hello, {name}!";
            IDictionary<string, object> data = new Dictionary<string, object> {{"name", "World"}};
            string result = renderer.Render(template, data);
            Assert.That(result, Is.EqualTo("Hello, World!"));
        }
        
        [Fact]
        public void RenderPlainStringCamelCaseVariable() {
            string template = "Hello, {theName}!";
            IDictionary<string, object> data = new Dictionary<string, object> {{"theName", "World"}};
            string result = renderer.Render(template, data);
            Assert.That(result, Is.EqualTo("Hello, World!"));
        }

        [Fact]
        public void RenderPlainNumericVariables() {
            string template = "Count is {count}!";
            IDictionary<string, object> data = new Dictionary<string, object> {{"count", 19}};
            string result = renderer.Render(template, data);
            Assert.That(result, Is.EqualTo("Count is 19!"));
        }
    }
}