using System.Collections.Generic;
using NHamcrest;
using PostOffice.Scriban;
using PostOffice.Templating;
using Xunit;
using Assert = NHamcrest.XUnit.Assert;

namespace PostOffice.Tests {
    public class ScribanTemplateRendererTest {
        private readonly TemplateRenderer renderer = new ScribanTemplateRenderer();

        [Fact]
        public void RenderPlainStringVariables() {
            string template = "Hello, {{name}}!";
            IDictionary<string, object> data = new Dictionary<string, object> {{"name", "World"}};
            string result = renderer.Render(template, data);
            Assert.That(result, Is.EqualTo("Hello, World!"));
        }

        [Fact]
        public void RenderPlainNumericVariables() {
            string template = "Count is {{count}}!";
            IDictionary<string, object> data = new Dictionary<string, object> {{"count", 19}};
            string result = renderer.Render(template, data);
            Assert.That(result, Is.EqualTo("Count is 19!"));
        }

        [Fact]
        public void RenderObjectProperty() {
            string template = "Hello, {{ctx.value}}!";
            IDictionary<string, object> data = new Dictionary<string, object> {
                {"ctx", new Container {Value = "World", SubContainer = new SubContainer {Value = "World"}}}
            };
            string result = renderer.Render(template, data);
            Assert.That(result, Is.EqualTo("Hello, World!"));
        }

        [Fact]
        public void RenderObjectDoubleNestedProperty() {
            string template = "Hello, {{ctx.subContainer.value}}!";
            IDictionary<string, object> data = new Dictionary<string, object> {
                {"ctx", new Container {Value = "World", SubContainer = new SubContainer {Value = "World"}}}
            };
            string result = renderer.Render(template, data);
            Assert.That(result, Is.EqualTo("Hello, World!"));
        }

        [Fact]
        public void RenderLists() {
            string template = @"
    Count is {{people | array.size }}
    People are
    {{ for person in people }}- {{person.name}}
    {{ end }}";
            IDictionary<string, object> data = new Dictionary<string, object> {
                {"people", new[] {Person("Pietro"), Person("Cristina"), Person("Irene"), Person("Laura")}}
            };
            string result = renderer.Render(template, data);
            Assert.That(result, Is.EqualTo(@"
    Count is 4
    People are
    - Pietro
    - Cristina
    - Irene
    - Laura
    "));
        }

        internal static Person Person(string name) => new Person {Name = name};
    }

    public class Container {
        public string Value { get; set; }
        public SubContainer SubContainer { get; set; }
    }

    public class SubContainer {
        public string Value { get; set; }
    }

    internal class Person {
        public string Name { get; set; }
    }
}