﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using PostOffice.Api;
using PostOffice.Attachments;
using PostOffice.Configuration;
using PostOffice.Sender;
using PostOffice.Templating;

namespace PostOffice.Runner {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Hello World!");
            var office = new PostOfficeService(new SmtpConfiguration {
                Host = "localhost",
                Port = 1025
            }, new LocalTemplateProvider(), new LocalAttachmentProvider(), new DefaultRenderer());

//            office.SendMail(new SendLetterCommand {
//                To = new[] { "you@me.com" },
//                LetterTemplate = "AAA",
//                EmbeddedAttachments = new[] {
//                    EmbeddedAttachment.From("XXX", "sample.gif", "image/gif")
//                },
//                Data = new Dictionary<string, object>()
//            }).ContinueWith(t => {
//                Console.WriteLine("Done.");
//                if (t.Exception != null) {
//                    Console.WriteLine(t.Exception.Message);
//                }
//            });
            
            office.SendMail(new SendLetterCommand {
                To = new[] { "aaaaa@me.com" },
                LetterTemplate = "AAA",
                Attachments = new Attachment[] {
                    // Attachment.From("XXX", "pippo.gif")
                },
                Data = new Dictionary<string, object>()
            }).ContinueWith(t => {
                Console.WriteLine("Done.");
                if (t.Exception != null) {
                    Console.WriteLine(t.Exception.Message);
                }
            });
            
            Console.ReadKey();
        }
    }

    internal class LocalAttachmentProvider : AttachmentProvider {
        public Task<Stream> ProvideAttachment(string attachmentId) {
            return Task.FromResult(
                new FileStream(@"C:\Users\pie\Desktop\BelatedComplexJerboa-small.gif", FileMode.Open) as Stream);
        }
    }

    internal class LocalTemplateProvider : TemplateProvider {
        public Task<EmailTemplate> ProvideTemplate(string templateName) {
            // return Task.FromResult(new EmailTemplate {
            //     From = "me@me.com",
            //     Subject = "AAA",
            //     HasHtmlContent = false,
            //     Body = "Zero\nUn\nDue\nTre\n\nCinque\n\nSette"
            // });
            
            return Task.FromResult(new EmailTemplate {
                From = "me@me.com",
                Subject = "AAA",
                HasHtmlContent = false,
                Body = @"Zero
Un
Due
Tre

Cinque

Sette
Otto"
            });
            
            
            // return Task.FromResult(new EmailTemplate {
            //     From = "me@me.com",
            //     Subject = "AAA",
            //     HasHtmlContent = true,
            //     Body = @"<img src=""cid:sample.gif"" /> Sample image"
            // });
        }
    }
}