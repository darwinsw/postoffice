using System.Threading.Tasks;

namespace PostOffice.Api {
    public interface PostOffice {
        Task SendMail(SendLetterCommand command);
    }
}