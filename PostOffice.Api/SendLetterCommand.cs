﻿using System.Collections.Generic;

namespace PostOffice.Api {
    public class SendLetterCommand {
        public IEnumerable<string> To { get; set; }
        public IEnumerable<string> Cc { get; set; }
        public IEnumerable<string> Bcc { get; set; }
        public string LetterTemplate { get; set; }
        public IDictionary<string, object> Data { get; set; }
        public IEnumerable<Attachment> Attachments { get; set; }
        public IEnumerable<EmbeddedAttachment> EmbeddedAttachments { get; set; }
    }

    public class Attachment {
        public string Id { get; set; }
        public string AttachmentName { get; set; }
        
        private Attachment(string id, string attachmentName) {
            Id = id;
            AttachmentName = attachmentName;
        }
        
        public static Attachment From(string id, string name) {
            return new Attachment(id, name);
        }
        public static Attachment From(string id) {
            return new Attachment(id, id);
        }
    }
    
    public class EmbeddedAttachment {
        private EmbeddedAttachment(string id, string attachmentName, string contentType) {
            Id = id;
            AttachmentName = attachmentName;
            ContentType = contentType;
        }

        public string Id { get; private set; }
        public string AttachmentName { get; private set; }
        public string ContentType { get; private set; }

        public static EmbeddedAttachment From(string id, string name, string contentType) {
            return new EmbeddedAttachment(id, name, contentType);
        }
        public static EmbeddedAttachment From(string id, string contentType) {
            return new EmbeddedAttachment(id, id, contentType);
        }
    }
}