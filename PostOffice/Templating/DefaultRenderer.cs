using System.Collections.Generic;
using System.Linq;

namespace PostOffice.Templating {
    public class DefaultRenderer : TemplateRenderer {
        public string Render(string src, IDictionary<string, object> data) {
            return data.Aggregate(src, (current, pair) 
                => current.Replace("{" + pair.Key + "}", pair.Value.ToString()));
        }
    }
}