using System.Collections.Generic;

namespace PostOffice.Templating {
    public interface TemplateRenderer {
        string Render(string template, IDictionary<string, object> data);
    }
}