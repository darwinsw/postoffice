﻿using System.Threading.Tasks;

namespace PostOffice.Templating {
    public interface TemplateProvider {
        Task<EmailTemplate> ProvideTemplate(string templateName);
    }
}