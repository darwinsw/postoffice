﻿using System;
using System.Threading.Tasks;

namespace PostOffice.Configuration {
    public interface SmtpConfigurationProvider {
        Task<SmtpConfiguration> ProvideConfiguration();
    }
}