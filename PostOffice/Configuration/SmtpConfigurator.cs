using System.Threading.Tasks;

namespace PostOffice.Configuration {
    public delegate Task<SmtpConfiguration> SmtpConfigurator();
}