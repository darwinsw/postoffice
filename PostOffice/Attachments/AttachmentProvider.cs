﻿using System.IO;
using System.Threading.Tasks;

namespace PostOffice.Attachments {
    public interface AttachmentProvider {
        Task<Stream> ProvideAttachment(string attachmentId);
    }
}