﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using PostOffice.Api;
using PostOffice.Attachments;
using Attachment = PostOffice.Api.Attachment;
using MailAttachment = System.Net.Mail.Attachment;
using PostOffice.Extensions;
using PostOffice.Configuration;
using PostOffice.Templating;

namespace PostOffice.Sender {
    public class PostOfficeService : Api.PostOffice {
        private readonly SmtpConfigurator configurator;
        private readonly TemplateProvider templateProvider;
        private readonly AttachmentProvider attachmentProvider;
        private readonly TemplateRenderer renderer;
        
        public PostOfficeService(SmtpConfigurator configurator, TemplateProvider templateProvider,
            AttachmentProvider attachmentProvider, TemplateRenderer renderer) {
            this.templateProvider = templateProvider;
            this.attachmentProvider = attachmentProvider;
            this.configurator = configurator;
            this.renderer = renderer ?? new DefaultRenderer();
        }

        public PostOfficeService(SmtpConfigurationProvider configurationProvider, TemplateProvider templateProvider,
            AttachmentProvider attachmentProvider, TemplateRenderer renderer) : this(configurationProvider.ProvideConfiguration, templateProvider, attachmentProvider, renderer) {
        }
        
        public PostOfficeService(SmtpConfiguration configuration, TemplateProvider templateProvider,
            AttachmentProvider attachmentProvider, TemplateRenderer renderer) : this(() => Task.FromResult(configuration), templateProvider, attachmentProvider, renderer) {
        }

        public async Task SendMail(SendLetterCommand command) {
            EmailTemplate template = await templateProvider.ProvideTemplate(command.LetterTemplate);
            SmtpClient client = await ConfigureSmtpClient();
            MailMessage message = await BuildEmailMessage(template, command);
            await client.SendMailAsync(message);
        }

        private async Task<MailMessage> BuildEmailMessage(EmailTemplate template, SendLetterCommand command) {
            var inputData = command.Data ?? new Dictionary<string, object>();
            var subject = renderer.Render(template.Subject, inputData);
            var body = renderer.Render(template.Body, command.Data);
            MailMessage message = new MailMessage {
                Subject = subject,
                Body = body,
                From = new MailAddress(template.From),
                IsBodyHtml = template.HasHtmlContent,
                BodyEncoding = Encoding.UTF8,
                BodyTransferEncoding = TransferEncoding.EightBit
            };

            foreach (string to in command.To) {
                message.To.Add(new MailAddress(to));
            }

            foreach (string to in command.Cc.OrEmpty()) {
                message.CC.Add(new MailAddress(to));
            }

            foreach (string to in command.Bcc.OrEmpty()) {
                message.Bcc.Add(new MailAddress(to));
            }

            if (command.Attachments != null && attachmentProvider != null) {
                foreach (Attachment attachment in command.Attachments) {
                    Stream stream = await attachmentProvider.ProvideAttachment(attachment.Id);
                    if (stream != null) {
                        message.Attachments.Add(new MailAttachment(stream, attachment.AttachmentName));
                    }
                }
            }
            
            if (command.EmbeddedAttachments != null && attachmentProvider != null) {
                AlternateView alternateView = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
                message.AlternateViews.Add(alternateView);
                foreach (EmbeddedAttachment attachment in command.EmbeddedAttachments) {
                    Stream stream = await attachmentProvider.ProvideAttachment(attachment.Id);
                    if (stream != null) {
                        LinkedResource res = new LinkedResource(stream);
                        res.ContentId = attachment.AttachmentName;
                        res.ContentType = new ContentType(attachment.ContentType);
                        alternateView.LinkedResources.Add(res);
                    }
                }
            }

            return message;
        }

        private async Task<SmtpClient> ConfigureSmtpClient() {
            SmtpConfiguration configuration = await configurator();

            SmtpClient client = new SmtpClient {
                Port = configuration.Port,
                Host = configuration.Host,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                EnableSsl = configuration.EnableSsl
            };
            if (configuration.RequiresAuthentication) {
                client.Credentials = new NetworkCredential(configuration.Username, configuration.Password);
            }

            return client;
        }
    }
}